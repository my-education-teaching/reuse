// var express = require("express");
// var router = express.Router();
// var log4 = require("../utils/log4j");
/* GET home page. */
module.exports = (router) => {
  // 默认测试页面
  router.get("/", function (req, res, next) {
    // log4.info("你好" + 123456);
    console.log(global.g_path.resolve(__dirname));
    res.render("index", { title: "续用应用", link: "https://websong.wang" });
  });

  return router;
};
