/* GET users listing. */
module.exports = (router) => {
  // 登录
  router.post("/enter");
  // 注册
  router.post("/register");
  // 找回密码
  router.post("/reset");
  // 退出
  router.post("/out");
  return router;
};
