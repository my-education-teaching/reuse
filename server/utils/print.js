module.exports = {
  log: function (mode, text) {
    console.log(mode, text);
  },
  default: "\x1b[30m",
  err: "\x1b[31m",
  ok: "\x1b[32m",
  warn: "\x1b[33m",
  blue: "\x1b[34m",
  red: "\x1b[35m",
};
