// 设置全局的变量&对象
module.exports = (val) => {
  if (Array.isArray(val)) {
    val.forEach((key) => {
      const name = Object.keys(key)[0];
      global["g_" + name] = key[name];
    });
  } else if (Object.prototype.toString.call(val) === "[object Object]") {
    const name = Object.keys(key)[0];
    global["g_" + name] = key[name];
  } else if (val) {
    global["g_" + val] = val;
  }
};
