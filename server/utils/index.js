// html转义
exports.html2Escape = function (sHtml) {
  return sHtml.replace(/[<>&"]/g, function (c) {
    return { "<": "&lt;", ">": "&gt;", "&": "&amp;", '"': "&quot;" }[c];
  });
};
