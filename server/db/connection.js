let mongoose = require("mongoose");
const { mongo } = require("@/config/env");
mongoose
  .createConnection(mongo)
  .on("open", () => {
    console.log("数据库连接成功！");
  })
  .on("error", () => {
    console.log("数据库连接失败！");
  })
  .on("close", () => {
    console.log("数据库关闭了！");
  });

module.exports = {};
