// let port = process.env.PORT || 4101;
// let mode = process.env.NODE_ENV || "init";
let mongoose = require("mongoose");
let print = require("../utils/print");
require("module-alias/register");
// 初始化数据库
const store = mongoose
  .createConnection("mongodb://localhost/reuse_app", {
    maxPoolSize: 10,
  })
  .on("close", () => print.log(print.err, "The database is shut down！"))
  .on("err", () => print.log(print.err, "The database open fail!"));

// 初始化用户表
const init_user = async function () {
  try {
    await store.dropCollection("users");
    await store.dropCollection("user_infos");
  } catch (err) {}
  const { users, user_infos } = require("@/schemas/users_schema.js");
  let userModule = await store.model("users", users(mongoose.Schema));
  let user_infos_module = await store.model(
    "user_infos",
    user_infos(mongoose.Schema)
  );
  const { user, info } = await require("@/resource/user_data.js");
  const _user = await userModule.create(user);
  if (Array.isArray(_user)) {
    const state = _user.map(async (item) => {
      var _info_ = JSON.parse(JSON.stringify(info));
      _info_.uid = item._id;
      const _user_info = await user_infos_module.create(_info_);
      return await userModule.findByIdAndUpdate(item._id, {
        info_id: _user_info._id,
      });
    });
    print.log(print.warn, "*****用户表【批量】初始化完毕******");
    return state;
  }
  info.uid = _user._id;
  const _user_info = await user_infos_module.create(info);
  const state = await userModule.findByIdAndUpdate(_user._id, {
    info_id: _user_info._id,
  });
  print.log(print.warn, "*****用户表初始化完毕******");
  return state;
};

// 初始化产品表
const init_product = async function () {
  try {
    await store.dropCollection("products");
    await store.dropCollection("product_contents");
  } catch (err) {}
  const { html2Escape } = require("@/utils/index.js");
  const {
    products,
    product_contents,
  } = require("@/schemas/products_schema.js");
  const { item, inner } = require("@/resource/products_data.js");
  const products_module = await store.model(
    "products",
    products(mongoose.Schema)
  );
  const product_contents_module = await store.model(
    "product_contents",
    product_contents(mongoose.Schema)
  );

  const _product = await products_module.create(item);
  inner.p_id = _product._id;
  inner.content = html2Escape(inner.content);
  const _product_inner = await product_contents_module.create(inner);
  const state = await products_module.findByIdAndUpdate(_product._id, {
    content_id: _product_inner._id,
  });
  print.log(print.blue, "*****产品表初始化完毕******");
  return state;
};

// 初始化类别
const init_category = async function () {
  try {
    await store.dropCollection("categories");
  } catch (err) {}
  const categories_model = store.model(
    "categories",
    require("../schemas/categories_schema.js")(mongoose.Schema)
  );
  const state = categories_model.create(
    require("../resource/categories_data.js")
  );
  print.log(print.default, "******分类初始化完毕******");
  return state;
};

// 初始化购物车
const init_carts = async function () {
  try {
    await store.dropCollection("carts");
  } catch (err) {}
  const carts_model = store.model(
    "carts",
    require("../schemas/carts_schema.js")(mongoose.Schema)
  );
  const state = carts_model.create(require("../resource/carts_data.js"));
  print.log(print.red, "******购物车初始化完毕******");
  return state;
};

// 初始化订单
const init_order = async function () {
  try {
    await store.dropCollection("orders");
  } catch (err) {}
  const orders_model = store.model(
    "orders",
    require("../schemas/orders_schema.js")(mongoose.Schema)
  );
  const state = orders_model.create(require("../resource/orders_data.js"));
  print.log(print.red, "******订单初始化完毕******");
  return state;
};

// 初始化聊天记录
const init_sms = async function () {
  try {
    await await store.dropCollection("sms");
  } catch (err) {}
  const sms_model = store.model(
    "sms",
    require("../schemas/sms_schema.js")(mongoose.Schema)
  );
  const state = sms_model.create(require("../resource/sms_data.js"));
  print.log(print.red, "******聊天记录初始化完毕******");
  return state;
};

// 初始化收藏
const init_collections = async function () {
  try {
    await store.dropCollection("collections");
  } catch (e) {}
  const collections_model = store.model(
    "collections",
    require("../schemas/collections_schema.js")(mongoose.Schema)
  );
  const state = collections_model.create(
    require("../resource/collections_data.js")
  );
  print.log(print.red, "******收藏初始化完毕******");
  return state;
};

// 初始化物流
const init_expressages = async function () {
  try {
    await store.dropCollection("expressages");
  } catch (e) {}
  const expressages_model = store.model(
    "expressages",
    require("../schemas/expressages_schema.js")(mongoose.Schema)
  );
  const state = expressages_model.create(
    require("../resource/expressages_data.js")
  );
  print.log(print.red, "******物流初始化完毕******");
  return state;
};

// 初始化评论
const init_comments = async function () {
  try {
    await store.dropCollection("comments");
  } catch (e) {}
  const comments_model = store.model(
    "comments",
    require("../schemas/comments_schema.js")(mongoose.Schema)
  );
  const state = comments_model.create(require("../resource/comments_data.js"));
  print.log(print.red, "******评论初始化完毕******");
  return state;
};

// 初始化关注和粉丝
const init_friends = async function () {
  try {
    await store.dropCollection("friends");
  } catch (e) {}
  const friends_model = store.model(
    "friends",
    require("../schemas/friends_schema.js")(mongoose.Schema)
  );
  const state = friends_model.create(require("../resource/friends_data.js"));
  print.log(print.red, "******关注和粉丝初始化完毕******");
  return state;
};
// 开始-初始化
store.on("open", async () => {
  print.log(print.ok, "database open successful！start init database……");
  // return false;

  await init_user();
  await init_product();
  await init_category();
  await init_carts();
  await init_order();
  await init_sms();
  await init_collections();
  await init_expressages();
  await init_comments();
  await init_friends();
});
