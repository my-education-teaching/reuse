# 续用程序说明
## 技术栈
nodejs14.17.3
 **详情看package.json**，以下摘选部分依赖
 ```json
 "dependencies": {
    "cookie-parser": "~1.4.4",
    "cross-env": "^7.0.3",
    "debug": "~2.6.9",
    "ejs": "~2.6.1",
    "express": "~4.16.1",
    "http-errors": "~1.6.3",
    "log4js": "^6.5.2",
    "module-alias": "^2.2.2",
    "mongoose": "^5.8.9",
    "morgan": "~1.9.1"
  }
 ```
## 结构
- middleware 中间件
- modules 数据库的模板模型
- resource 资源-数据库初始数据
- schemas 数据库的类型
- utils 工具
- config 配置
- routes 路由
- controls 业务层-逻辑
- public 静态资源目录
- views 静态页面
- dev-docs 开发过程中的笔记

## 数据库
- mongodb4.2
## 功能

## 接口文档