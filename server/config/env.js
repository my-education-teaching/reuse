// 环境变量
const ENV = process.env.NODE_ENV;

const ips = {
  development: {
    mongo: "mongodb://localhost/reuse_app",
  },
  production: {
    mongo: "mongodb://localhost/reuse_app",
  },
};

module.exports = ips[ENV];
