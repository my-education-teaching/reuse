exports.users = function (TYPE) {
  return new TYPE(
    {
      name: {
        type: String,
        required: [true, "账户不可空"],
        maxlength: 10,
        minlength: 2,
      },
      pass: {
        type: String,
        require: [true, "密码不可空"],
        minlength: 6,
        maxlength: 64,
      },
      token: {
        type: String,
        default: "",
      },
      info_id: {
        // 用户信息表的id
        type: TYPE.ObjectId,
        ref: "user_infos",
      },
    },
    { collection: "users" }
  );
};

// 用户信息子表
exports.user_infos = function (TYPE) {
  return new TYPE(
    {
      avatar: {
        type: String,
        maxlength: 300,
      },
      uid: {
        // 用户表，用户id
        type: TYPE.ObjectId,
        ref: "users",
      },
      phone: {
        type: String,
        default: "",
      },
      email: {
        type: String,
        default: "",
      },
      gender: {
        type: Number,
        default: -1,
      },
      address: {
        type: Array,
      },
      auth: {
        type: Boolean,
        default: false,
      },
      card_bank: {
        type: Array,
      },
      vip: {
        type: Object,
        default: {
          create_time: "",
          grade: -1,
          time_out: "",
        },
      },
      punch_card: {
        type: Number,
        default: 0,
      },
      integral: {
        type: Number,
        default: 0,
      },
      create_time: {
        type: Date,
        default: Date.now,
      },
      birthday: {
        type: String,
        default: "",
      },
      state: {
        type: Number,
        default: 0,
      },
      mood: {
        type: String,
        default: "今天心情是？",
        maxlength: 150,
      },
      update_time: {
        type: Date,
        default: Date.now,
      },
    },
    {
      collection: "user_infos",
      timestamps: { createdAt: "create_time", updatedAt: "update_time" },
    }
  );
};
