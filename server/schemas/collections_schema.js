module.exports = function (Schema) {
  return new Schema(
    {
      s_id: {
        type: String,
        required: [true, "商品id不可空"],
      },
      s_name: {
        type: String,
        required: [true, "商品名称不可空"],
      },
      s_icon: {
        type: String,
        required: [true, "商品图片不可空"],
      },
      uid: {
        type: String,
        required: [true, "收藏者id不可空"],
      },
      pid: {
        type: String,
        required: [true, "商品发布者id不可空"],
      },
      date: {
        type: Date,
        default: Date.now,
      },
    },
    {
      collection: "collections",
      timestamps: { createdAt: "date" },
    }
  );
};
