module.exports = function (Schema) {
  return new Schema(
    {
      e_code: {
        type: String,
        required: [true, "物流编号不可空"],
      },
      e_type: {
        type: String,
        required: [true, "快递类型不可空"],
      },
      seller_id: {
        type: String,
        required: [true, "卖家id不可空"],
      },
      buyer_id: {
        type: String,
        required: [true, "买家id不可空"],
      },
      date: {
        type: Date,
        default: Date.now,
      },
    },
    {
      collection: "expressages",
      timestamps: { createdAt: "date" },
    }
  );
};
