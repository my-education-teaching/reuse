module.exports = function (Schema) {
  return new Schema(
    {
      s_id: String,
      s_price: Number,
      s_count: Number,
      s_icon: String,
      s_name: String,
      s_seller_id: String,
      s_buyer_id: String,
    },
    {
      collection: "carts",
    }
  );
};
