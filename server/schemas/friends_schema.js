module.exports = function (Schema) {
  return new Schema(
    {
      follower_id: String,
      follower_name: String,
      follower_avatar: String,
      be_followed_id: String,
      be_followed_name: String,
      be_followed_avatar: String,
    },
    {
      collection: "friends",
    }
  );
};
