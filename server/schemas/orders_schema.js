module.exports = function (Schema) {
  return new Schema(
    {
      order_code: String,
      s_icon: String,
      s_id: String,
      s_price: Number,
      s_count: Number,
      s_$: Number,
      s_seller_id: String,
      s_buyer_id: String,
      state: {
        type: Number,
        default: 0,
      },
    },
    {
      collection: "orders",
    }
  );
};
