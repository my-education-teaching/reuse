module.exports = function (Schema) {
  return new Schema(
    {
      s_id: String,
      u_id: String,
      date: {
        type: Date,
        default: Date.now,
      },
      state: {
        type: Number,
        default: -1,
      },
      zan: {
        type: Number,
        default: 0,
      },
      cai: {
        type: Number,
        default: 0,
      },
      content: String,
      star: Number,
      reply: {
        type: [
          {
            r_id: String,
            date: {
              type: Date,
              default: Date.now,
            },
            zan: Number,
            cai: Number,
            content: String,
            state: {
              type: Number,
              default: -1,
            },
          },
        ],
      },
    },
    {
      collection: "comments",
      timestamps: { createdAt: "date" },
    }
  );
};
