module.exports = function (Schema) {
  return new Schema(
    {
      sender_id: String,
      recipient_id: String,
      state: {
        type: Number,
        default: 0,
      },
      category: {
        type: String,
        default: "private",
      },
      content: String,
      date: {
        type: Date,
        default: Date.now,
      },
    },
    {
      collection: "sms",
      timestamps: { createdAt: "date" },
    }
  );
};
