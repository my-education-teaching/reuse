module.exports = function (Schema) {
  return new Schema(
    {
      category_name: {
        type: String,
        required: [true, "分类名不可空"],
        maxlength: 10,
        minlength: 2,
      },
      hot: {
        type: Boolean,
        default: false,
      },
      key: {
        type: String,
        default: "",
      },
    },
    {
      collection: "categories",
    }
  );
};
