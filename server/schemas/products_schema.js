// 产品
exports.products = function (Schema) {
  return new Schema(
    {
      title: {
        type: String,
        required: [true, "标题不可空"],
      },
      price: {
        type: Number,
        required: [true, "价格不可空"],
      },
      o_price: {
        type: Number,
      },
      discount: {
        type: [String, Number],
        default: "没有折扣",
      },
      s_count: {
        type: Number,
        default: 0,
      },
      b_count: {
        type: Number,
        default: 0,
      },
      r_count: {
        type: Number,
        default: 0,
        required: [true, "库存不可空"],
      },
      position: {
        type: Object,
        required: [true, "发货地不可空"],
        default: { coord: { type: Array }, name: String },
      },
      content_id: {
        type: Schema.ObjectId,
        ref: "product_contents",
      },
      icon: {
        type: Object,
        required: [true, "缩略图不可空"],
        default: {
          root: "/images/products/",
          src: "",
        },
      },
      banner: {
        type: Array,
      },
      uid: {
        type: Schema.Types.Mixed,
        required: [true, "用户id不可空"],
      },
      uname: {
        type: String,
        required: [true, "用户名不可空"],
      },
      state: Number,
      create_time: {
        type: Date,
        default: Date.now,
      },
      issued_time: {
        type: Date,
      },
      update_time: {
        type: Date,
        default: Date.now,
      },
      category_id: {
        type: Array,
        required: [true, "分类不可空"],
        default: [],
      },
    },
    {
      collection: "products",
      timestamps: { createdAt: "create_time", updatedAt: "update_time" },
    }
  );
};

// 产品详情内容
exports.product_contents = function (Schema) {
  return new Schema(
    {
      p_id: {
        type: Schema.ObjectId,
        ref: "products",
      },
      content: {
        type: Schema.Types.Mixed,
      },
      pics: {
        type: Array,
      },
    },
    {
      collection: "product_contents",
    }
  );
};
