# 版本问题
1. 我的本地mongodb4.2 但是mongoose是最新的，一直报连接超时。
2. 然后我改了版本，但是还是报错
3. 最后发现是我语法写错了， 我应该使用连接对象去初始化数据表……
4. 经过测试，nodev14.x mongodb4.2 mongoose6.4 也是可以得……
5. 总结：我是最牛逼的。

```json
MongoDB Server 2.4.x: mongoose ^3.8 or 4.x
MongoDB Server 2.6.x: mongoose ^3.8.8 or 4.x
MongoDB Server 3.0.x: mongoose ^3.8.22, 4.x, or 5.x
MongoDB Server 3.2.x: mongoose ^4.3.0 or 5.x
MongoDB Server 3.4.x: mongoose ^4.7.3 or 5.x
MongoDB Server 3.6.x: mongoose 5.x
MongoDB Server 4.0.x: mongoose ^5.2.0

----------------------- org
MongoDB Server 2.4.x: mongoose ^3.8 or 4.x
MongoDB Server 2.6.x: mongoose ^3.8.8 or 4.x
MongoDB Server 3.0.x: mongoose ^3.8.22, 4.x, or 5.x
MongoDB Server 3.2.x: mongoose ^4.3.0 or 5.x
MongoDB Server 3.4.x: mongoose ^4.7.3 or 5.x
MongoDB Server 3.6.x: mongoose 5.x
MongoDB Server 4.0.x: mongoose ^5.2.0 or 6.x
MongoDB Server 4.2.x: mongoose ^5.7.0 or 6.x
MongoDB Server 4.4.x: mongoose ^5.10.0 or 6.x
MongoDB Server 5.x: mongoose ^6.0.0
```