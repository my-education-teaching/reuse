/* File Info 
 * Author:      宋宇 
 * CreateTime:  6/20/2022, 8:45:25 PM 
 * LastEditor:  宋宇 
 * ModifyTime:  6/20/2022, 8:45:32 PM 
 * Description: log4js 使用笔记
 * version：^6.5.2
*/ 

```js
// 1. 配置
log4js.configure({
  // 存放器
  appenders: {
    // 自定义的存储类型和方式
    console: {
      // 输出类型:console就是输出到控制台
      type: "console",
    },
    // info或者console，名字自定义，用于给categories分类使用
    info: {
      // dataFile输出到文件里
      type: "dateFile",
      // 文件名
      filename: path.join(__dirname, `/../logs/${fileName}`),
      // 将模式写入到日志中
      alwaysIncludePattern: true,
      // node14下，这个配置报警，注释掉了
      // daysToKeep: 10,
      // 文件名的格式化
      pattern: "_info_yyyy-MM-dd-hh.log",
      // 编码格式
      encoding: "utf-8",
    },
  },
  categories: {
    // 默认分类不能删，必须有
    default: {
      // 选择一个输出模式，自定义的
      appenders: ["console"],
      // 权重：【注意，这里的权重决定了，使用时候的权重，如果使用的权重小于设置的权重，则不能生效】
      level: "DEBUG",
    },
    // 发布模式下，写入文件，上面是开发模式，只会打印到控制台
    production: {
      appenders: ["info"],
      // 等级：ALL <DEBUG<INFO<WARN<ERROR<FATAL<OFF。
      level: "all",
    },
  },
});
// 根据环境变量，来控制日志的输出方式，下面就是全局的选择分类的方式
let logger;
if (process.env.NODE_ENV == "development") {
  logger = log4js.getLogger("console");
} else {
  logger = log4js.getLogger("production");
}
module.exports = logger;
```