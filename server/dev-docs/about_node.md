# nodejs的一些注意事项
1. require函数加载的模块会常驻内存，所以我开发时尽量使用全局注入或使用入参形式复用

```js 
// 使用全局变量
global.app = app

```