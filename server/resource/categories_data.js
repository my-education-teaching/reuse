module.exports = [
  {
    category_name: "书籍",
    hot: true,
    key: "liber_001",
  },
  {
    category_name: "电子",
    hot: true,
    key: "elect_001",
  },
  {
    category_name: "玩具",
    hot: true,
    key: "toy_001",
  },
  {
    category_name: "工具",
    hot: true,
    key: "tools_001",
  },
];
