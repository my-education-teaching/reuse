# 续用平台

## server
- stack ： nodejs 14.17.3  express@4 mongodb@4.2 
- scripts ：init 初始化数据库-第一次使用

## web
- 前端项目：vite+vue3+paina

## doc
- 开发文档和手记

## admin
- 后台管理系统

## tip
该项目开源了，虎头蛇尾，我可能要长期或许永久离开程序员行列了，如果未来有机会，我会回来继续得，如果您想参与该项目开发，很欢迎，请给我发邮件我来邀请您。

qq: 289483936

wechat: web_songyu

author：宋宇

date：2022年7月19日